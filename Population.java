import java.util.List;
import java.util.Random;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

class Population implements Iterable<Member>{
    List<Member> members;
    int numberMembers;

    public Population(int numberMembers,int memberSize){
        this.numberMembers=numberMembers;
        members = new ArrayList<Member>();
        for(int i=0; i<numberMembers; i++){
            members.add(new Member(memberSize));
        }
    }

    public void sort(){
        // Sort population from heighest fitness to lowest
        Collections.sort(members,Collections.reverseOrder());
    }

    public int size(){
        return members.size();
    }

    public Member get(int index){
        return members.get(index);
    }

    public void set(int index,Member member){
        members.set(index,member);
    }

    public void removeLast(){
        // System.out.print(members.get(size()-1).fitness);
        members.remove(size()-1);
    }

    public void addAll(List<Member> members){
        this.members.addAll(members);
    }

    public Iterator<Member> iterator(){
        return members.iterator();
    }

    public void print(){
        for(Member member : members){
            member.print();
        }
    }

    public Member getMemberBasedOnFitness(){
        // Members with a high fitness have a bigger chance to get selected
        for(Member member : members){
            if(new Random().nextDouble()<(4.0/numberMembers)){
                return member;
            }
        }
        return members.get(0);
    }
    
}