import org.vu.contest.ContestSubmission;
import org.vu.contest.ContestEvaluation;
import java.util.Random;
import java.util.Properties;
import java.util.List;
import java.util.ArrayList;
import java.util.Collections;
import java.util.stream.IntStream;
import java.io.PrintStream;
import java.net.PortUnreachableException;

public class player0 implements ContestSubmission {
	Random rnd_;
	ContestEvaluation evaluation_;
	int dimensions = 10;
	private int evaluations_limit_;

	public player0() {
		rnd_ = new Random();
		run();
	}

	public static void main(String[] args) {
		new player0();
	}

	public void setSeed(long seed) {
		// Set seed of algortihms random process
		rnd_.setSeed(seed);
	}

	public void setEvaluation(ContestEvaluation evaluation) {
		// Set evaluation problem used in the run

		evaluation_ = evaluation;

		
		Member.evaluation = evaluation;

		// Get evaluation properties
		Properties props = evaluation.getProperties();
		// Get evaluation limit
		evaluations_limit_ = Integer.parseInt(props.getProperty("Evaluations"));
		// Property keys depend on specific evaluation
		// E.g. double param = Double.parseDouble(props.getProperty("property_name"));
		boolean isMultimodal = Boolean.parseBoolean(props.getProperty("Multimodal"));
		boolean hasStructure = Boolean.parseBoolean(props.getProperty("Regular"));
		boolean isSeparable = Boolean.parseBoolean(props.getProperty("Separable"));

		// Do sth with property values, e.g. specify relevant settings of your algorithm
		if (isMultimodal) {
			// Do sth
		} else {
			// Do sth else
		}
	}

	public void run() {

		Member.player=this;

		// String algorithm = "g";
		 String algorithm = "d";
		// String algorithm = System.getProperty("algorithm");
		System.out.print("running "+algorithm+"\n");

		switch(algorithm){
			case "d": differentialEvolution();
			break;
			case "g": geneticAlgorithm();
			break;
		}
	}
	public void differentialEvolution(){
		// Get parameters from system
		// int populationSize = Integer.parseInt(System.getProperty("populationsize"));	
		// double F = Double.parseDouble(System.getProperty("F"));	
		// double CR = Double.parseDouble(System.getProperty("CR"));	

		int populationSize = 10;
		int F = 0.6; // differential weight
		double CR = 0.6; // crossover rate

		int evals=0;

		// init population
		Population population = new Population(populationSize,dimensions);

		while (evals < evaluations_limit_) {
			
			int index=0;
			for(Member member : population){

				List<Member> parents = new ArrayList<Member>();
				Member child = new Member(dimensions);

				while(parents.size()<3){
					int randomIndex = new Random().nextInt(populationSize);
					Member parent = population.get(randomIndex);

					//Check if parent is not equal to child and if not already in parents
					if(!parent.equals(member)&&!parents.contains(parent)){
						parents.add(parent);
					}
				}

				int R = new Random().nextInt(dimensions);


				for(int i=0; i<dimensions; i++){
					double r_i = new Random().nextDouble();

					if(r_i<CR || i==R){
						double parent1Value=parents.get(0).get(i);
						double parent2Value=parents.get(1).get(i);
						double parent3Value=parents.get(2).get(i);
						double value = parent1Value+F*(parent2Value-parent3Value);
						if(value<5&&value>-5){
							child.set(i,value);
						}else{
							child.set(i,member.get(i));
						}
					}else{
						child.set(i,member.get(i));
					}
				}

				member.setFitness();
				child.setFitness();
				evals+=2;
				if(child.fitness>member.fitness){
					population.set(index,child);
				}
				index++;
			}
				
		}
			
	}
	
	
	public void geneticAlgorithm(){
		// Get parameters from System
		Member.mutationMagnitude=Double.parseDouble(System.getProperty("MM"));
		Member.mutationRate=Double.parseDouble(System.getProperty("MR"));
		int populationSize = Integer.parseInt(System.getProperty("populationsize"));	
		int numberParents = Integer.parseInt(System.getProperty("parents"));
		
		// Member.mutationMagnitude=1;
		// Member.mutationRate=0.5;
		// int populationSize = 20;
		// int numberParents = 5;

		int valuesPerMember = 10;
		int evals=0;

		// init population
		Population population = new Population(populationSize,valuesPerMember);

		// Not sure: using the value evaluations_limit makes sure that evaluation is set?
		OUTERLOOP:
		while (evals < evaluations_limit_) {
			
			// Calculate fitness for each member
			for(Member member : population){
				member.setFitness();
				evals++;
				if(evals == evaluations_limit_){
					break OUTERLOOP;
				}
			}

			// Sort population from best fitness to worst fitness
			population.sort();
			
			// Remove last member if population exceeded the population size 
			while(population.size()>populationSize){
				population.removeLast();
			}

			// Remove worst x percent
			// for(int i = 0; i <= 0.2*populationSize; i++){
			// 	population.removeLast();
			// }

			Population newPopulation = new Population(populationSize,valuesPerMember);

			// Select parents based on their fitness
			// Crossover these parents and create a population twice the size of the original population
			while(newPopulation.size()<populationSize*2){
				List<Member> parents = new ArrayList<Member>();
				while(parents.size()<numberParents){
					Member parent = population.getMemberBasedOnFitness();
					if(!parents.contains(parent)){
						parents.add(parent);
					}
				}
				List<Member> children = geneticCrossover(parents);
				newPopulation.addAll(children);
			}
			// Replace old population by new population
			population=newPopulation;
			
			// Mutate all members
			for(Member member : population){
				member.mutate();
			}
		}
	}

    public List<Member> geneticCrossover(List<Member> parents){
		// Example of genetic crossover:
		// input: 0,0,0,0,0,0 and 1,1,1,1,1,1
		// crossOverpoint: 3
		// output: 0,0,0,1,1,1 and 1,1,1,0,0,0

		int numberValues=parents.get(0).numberValues();

		// Generate crossover points
		List<Integer> crossoverPoints = new ArrayList<Integer>();
		while(crossoverPoints.size()<parents.size()-1){
			int point = 1+new Random().nextInt(numberValues-2);
			if(!crossoverPoints.contains(point)){
				crossoverPoints.add(point);
			}
		}

		// Sort crossover points ascending order
		Collections.sort(crossoverPoints);

		// Add maximum index to crossoverpoints
		crossoverPoints.add(crossoverPoints.size(),numberValues-1);

		List<Member> children = new ArrayList<Member>();

		// Add parents values to children according to example
		for(int i=0; i<parents.size(); i++){
			Member child = new Member(parents.get(0));
			for(int j=0; j<crossoverPoints.size()-1; j++){
				for(int k=crossoverPoints.get(j); k<=crossoverPoints.get(j+1); k++){
					child.set(k, parents.get(j+1).get(k));
				}

			}
			children.add(child);

			// Move first parent to last spot
			parents.add(parents.get(0));
			parents.remove(0);
		}

		return children;
    }

	public double[][] initPopulation(int size) {
		double population[][] = new double[size][];
		for (int i = 0; i < size; i++) {
			double member[] = new double[dimensions];

			for (int j = 0; j < dimensions; j++) {
				member[j] = getNewRandomNumberBetween(-5, 5);
			}

			population[i] = member;
		}

		return population;
	}

	public double getNewRandomNumberBetween(double lower, double upper) {
		if (lower < 0 && upper < 0) {
			return lower + (Math.abs(lower) - Math.abs(upper)) * rnd_.nextDouble();
		} else if (lower < 0) {
			return lower + (Math.abs(lower) + upper) * rnd_.nextDouble();
		}

		return lower + (upper - lower) * rnd_.nextDouble();
	}

	public double[] calculateFitnessesOfPopulation(double[][] population) {
		double fitnesses[] = new double[population.length];

		for (int i = 0; i < population.length; i++) {
			double[] member = population[i];
			double fitness = evaluate(member);
			// double fitness = (double) evaluation_.evaluate(member);

			fitnesses[i] = fitness;
		}

		return fitnesses;
	}

	public double[][]getXDifferentRandomAgents(int x, double[][] population, int currentAgent) {
		int[] indexes = new int[x];
		int filledIndexes = 0;
		while (filledIndexes < x) {
			int newIndex = (int) getNewRandomNumberBetween(0, population.length);

			if (!IntStream.of(indexes).anyMatch(y -> y == newIndex) && newIndex != currentAgent) {
				indexes[filledIndexes] = newIndex;
				filledIndexes += 1;
			}
		}

		double[][] xRandomAgents = new double[x][];

		for (int i = 0; i < x; i++) {
			xRandomAgents[i] = population[indexes[i]];
		}
		return xRandomAgents;
	}

	public boolean outOfRange(double[] child){
		for(int i = 0; i < child.length; i++){
			if (child[i] > 5){
				return true;
			} else if (child[i] < -5){
				return true;
			}
		}
		return false;
	}

    public double evaluate(double[] member)
    {
        return (double) evaluation_.evaluate(member);
	}
	
	public void printEvaluation(int getal)
	{
		new java.io.PrintStream(System.out).print(getal);
		new java.io.PrintStream(System.out).print("Eval:");
		new java.io.PrintStream(System.out).print(evaluation_);
		new java.io.PrintStream(System.out).print("\n");
	}
}
