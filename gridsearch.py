import os
import subprocess
import re
import numpy as np
import pandas as pd

functions=['BentCigarFunction','SchaffersEvaluation','KatsuuraEvaluation']
#functions=['BentCigarFunction','SchaffersEvaluation']

def repeat_search_genetic(filename):
    result_df = pd.DataFrame(columns = ["parents", "best_pop", "best_mr", "best_mm", "cigarsd", "cigaravg","schaffersd", "schafferavg" ,"katsuurasd", "katsuuraavg","avg_scoresd", "avg_scoreavg"])
    df = pd.read_csv(filename, header = 0)
    df = df.drop(list(df)[0],1)
    #print(df)
    for i in range(2,6):
        new_df = df.loc[df['parents'] == i]
        new_df = new_df.loc[new_df['best_score']>new_df['best_score'].quantile(q = 0.95 )]
        for j in new_df.index.values.tolist():
            #print(new_df['best_pop'][j])
            
#            [cigar,schaffer,katsuura, averag] = runGenetic(new_df.at[j,'best_pop'],new_df.at[j,'parents'],new_df.at[j,'best_mr'], new_df.at[j,'best_mm'])
            result_df = result_df.append(pd.DataFrame(runGenetic(new_df.at[j,'best_pop'],new_df.at[j,'parents'],new_df.at[j,'best_mr'], new_df.at[j,'best_mm']),columns = ["parents", "best_pop", "best_mr", "best_mm", "cigarsd", "cigaravg","schaffersd", "schafferavg" ,"katsuurasd", "katsuuraavg","avg_scoresd", "avg_scoreavg"]))
        result_df = result_df.append(pd.DataFrame([[0,0,0,0,0,0,0,0,0,0,0,0]],columns = ["parents", "best_pop", "best_mr", "best_mm", "cigarsd", "cigaravg","schaffersd", "schafferavg" ,"katsuurasd", "katsuuraavg","avg_scoresd", "avg_scoreavg"]))
    return result_df
    
def repeat_search_differential(filename):
    result_df = pd.DataFrame(columns = ["best_pop", "best_f", "best_cr", "cigarsd", "cigaravg","schaffersd", "schafferavg" ,"katsuurasd", "katsuuraavg","avg_scoresd", "avg_scoreavg"])
    df = pd.read_csv(filename, header = 0)
    df = df.drop(list(df)[0],1)
    new_df = df.loc[df['best_score']>df['best_score'].quantile(q = 0.95 )]
    for j in new_df.index.values.tolist():
        #[cigar,schaffer,katsuura, averag] = runDifferential(new_df.at[j,'best_pop'],new_df.at[j,'best_f'], new_df.at[j,'best_cr'])
        result_df = result_df.append(pd.DataFrame(runDifferential(new_df.at[j,'best_pop'],new_df.at[j,'best_f'], new_df.at[j,'best_cr']),columns = ["best_pop", "best_f", "best_cr", "cigarsd", "cigaravg","schaffersd", "schafferavg" ,"katsuurasd", "katsuuraavg","avg_scoresd", "avg_scoreavg"]))
    return result_df
    
def findScore(input):
    # attains score from java console output
    result = re.search(r'Score: (.*?)\\r', str(input))
    return float(result.group(1))

def runDifferential(population,f,cr):
    cigar = []
    schaffer = []
    katsuura = []
    final_score = []
    for i in range(10):
        score=0
        for function in functions:
            call="java -Dalgorithm=d -Dpopulationsize="+str(int(population))+" -DF="+str(f)+" -DCR="+str(cr)+" -jar testrun.jar  -submission=player0 -evaluation="+function+" -seed=1"
            output = findScore(subprocess.check_output(call, shell=True))
            #output = 1
            score += output
            #score+=findScore(output)
            if function == 'BentCigarFunction':
                cigar.append(output)
            elif function =='SchaffersEvaluation':
                schaffer.append(output)
            elif function == 'KatsuuraEvaluation':
                katsuura.append(output)
        final_score.append(score/len(functions))
    # return average algorithm score
    return [[population,f,cr, np.std(cigar),np.mean(cigar),np.std(schaffer),np.mean(schaffer), np.std(katsuura),np.mean(katsuura),np.std(final_score), np.mean(final_score)]]

    


def runGenetic(population,parents,mr,mm):
    cigar = []
    schaffer = []
    katsuura = []
    final_score = []
    #print("hi")
    for i in range(10):
        score=0
        for function in functions:
            call="java -Dalgorithm=g -Dpopulationsize="+str(int(population))+" -Dparents="+str(parents)+" -DMR="+str(mr)+" -DMM="+str(mm)+" -jar testrun.jar  -submission=player0 -evaluation="+function+" -seed=1"
            #print("did_something")
            #output = 1
            output = findScore(subprocess.check_output(call, shell=True))
            if function == 'BentCigarFunction':
                cigar.append(output)
            elif function =='SchaffersEvaluation':
                schaffer.append(output)
            elif function == 'KatsuuraEvaluation':
                katsuura.append(output)
            #score+=findScore(output)
            score += output
        final_score.append(score/len(functions))
    # return average algorithm score
    
    return [[parents,population,mr,mm, np.std(cigar),np.mean(cigar),np.std(schaffer),np.mean(schaffer), np.std(katsuura),np.mean(katsuura),np.std(final_score), np.mean(final_score)]]



def grid_search(threshold = 0.000, algorithm = "Genetic"):
    if algorithm == "Genetic":
        df= pd.DataFrame(columns = ["parents", "best_pop", "best_mr", "best_mm", "best_score"])
        for i in range(2,6):
            parents = i
            best_pop = int(10)
            best_mr = 0.0
            best_mm = 0.0
            best_score= runGenetic(best_pop,parents, best_mr,best_mm)
            for pop in np.linspace(10,210,5):
                prev_score = runGenetic(pop,parents,0.0,0.0)
                for mr in np.linspace(0.1,1,10):
                    prev_next_score = runGenetic(pop,parents,mr,0.0)
                    for mm in np.linspace(0.2,1,5):
                        next_score = runGenetic(pop,parents,mr,mm)
                        df = df.append(pd.DataFrame([[parents, pop, mr, mm, next_score]],columns = ["parents", "best_pop", "best_mr", "best_mm", "best_score"]))
                        if next_score < prev_next_score + threshold:
                            break
                        else: prev_next_score = next_score
                    if prev_next_score < prev_score:# + threshold:
                        pass #break
                    else: prev_score = prev_next_score
                if prev_score > best_score:
                    best_score = prev_score
                    best_pop = pop
                    best_mr = mr
                    best_mm = mm
            df = df.append(pd.DataFrame([[parents, best_pop, best_mr, best_mm, best_score],[0,0,0,0,0]],columns = ["parents", "best_pop", "best_mr", "best_mm", "best_score"]))
            print("Genetic score for ", parents, ": ", best_score, "with settings: ", best_pop, best_mr, best_mm)

    elif algorithm == "Differential":
        df = pd.DataFrame(columns = ["best_pop", "best_f", "best_cr", "best_score"])
        best_pop = 10
        best_f = 0.0
        best_cr = 0.0
        best_score= runDifferential(best_pop, best_f,best_cr)
        for pop in np.linspace(10,210,5):
            prev_score = runDifferential(pop,0.0,0.0)
            for f in np.linspace(0.2,2,10):
                prev_next_score = runDifferential(pop,f,0.0)
                for cr in np.linspace(0.2,1,5):
                    next_score = runDifferential(pop,f,cr)
                    df = df.append(pd.DataFrame([[pop,f, cr, next_score]],columns = ["best_pop", "best_f", "best_cr", "best_score"] ))
                    if next_score < prev_next_score + threshold:
                        break
                    else: prev_next_score = next_score
                if prev_next_score < prev_score:# + threshold:
                    pass #break
                else: prev_score = prev_next_score
            if prev_score > best_score:
                best_score = prev_score
                best_pop = pop
                best_f = f
                best_cr = cr
        df = df.append(pd.DataFrame([[best_pop,best_f, best_cr, best_score],[0,0,0,0]],columns = ["best_pop", "best_f", "best_cr", "best_score"] ))
    return df
#print(runDifferential(10,0.4,0.2)) 
#filename = os.getcwd() + "\genetic_results.csv"
#df =  repeat_search_genetic(filename)
#df.to_csv("genetic_results_total_new.csv")
#filename = os.getcwd() + "\differential_results.csv"
#df =  repeat_search_differential(filename)
#df.to_csv("differential_results_new.csv")
#print(runGenetic(10,2,0.4,0.2))
df = pd.DataFrame( columns = ["parents", "best_pop", "best_mr", "best_mm", "cigarsd", "cigaravg","schaffersd", "schafferavg" ,"katsuurasd", "katsuuraavg","avg_scoresd", "avg_scoreavg"])
df = df.append(pd.DataFrame(runGenetic(210,2,0.1,0.4),columns = ["parents", "best_pop", "best_mr", "best_mm", "cigarsd", "cigaravg","schaffersd", "schafferavg" ,"katsuurasd", "katsuuraavg","avg_scoresd", "avg_scoreavg"]))
df = df.append(pd.DataFrame(runGenetic(110,3,0.1,0.6),columns = ["parents", "best_pop", "best_mr", "best_mm", "cigarsd", "cigaravg","schaffersd", "schafferavg" ,"katsuurasd", "katsuuraavg","avg_scoresd", "avg_scoreavg"]))
df = df.append(pd.DataFrame(runGenetic(210,4,0.1,0.4),columns = ["parents", "best_pop", "best_mr", "best_mm", "cigarsd", "cigaravg","schaffersd", "schafferavg" ,"katsuurasd", "katsuuraavg","avg_scoresd", "avg_scoreavg"]))
df = df.append(pd.DataFrame(runGenetic(210,5,0.1,0.8),columns = ["parents", "best_pop", "best_mr", "best_mm", "cigarsd", "cigaravg","schaffersd", "schafferavg" ,"katsuurasd", "katsuuraavg","avg_scoresd", "avg_scoreavg"]))
df.to_csv("genetic_results_new.csv")
df = pd.DataFrame( runDifferential(10,0.6,0.4),columns = ["best_pop", "best_f", "best_cr", "cigarsd", "cigaravg","schaffersd", "schafferavg" ,"katsuurasd", "katsuuraavg","avg_scoresd", "avg_scoreavg"])
df.to_csv("differential_results_new.csv")
# Gridsearch:
