import org.vu.contest.ContestEvaluation;
import java.util.Random;
import java.util.List;
import java.util.ArrayList;

class Member implements Comparable<Member>{
    static ContestEvaluation evaluation;
    static player0 player;
    static int idCount=0;
    static double mutationRate;
    static double mutationMagnitude;

    int id;
    List<Double> values;
    double fitness;

    public Member(int numberValues){
        idCount++;
        id=idCount;
        values = new ArrayList<Double>();
        for(int i=0; i<numberValues; i++){
            values.add(getRandomValue());
        }
    }

    public Member(Member other){
        values=new ArrayList<Double>(other.values);
        idCount++;
        id=idCount;
    }

    public void set(int index, double value){
        values.set(index, value);
    }

    public double get(int index){
        return values.get(index);
    }

    private double getRandomValue(){
        return new Random().nextDouble()*10-5;
    }

    public void setFitness(){
        double[] valueArray = new double[numberValues()];
        for(int i = 0 ; i<numberValues(); i++){
            valueArray[i]=values.get(i);
        }
        // fitness = player.evaluate(valueArray);
        fitness = (double) evaluation.evaluate(valueArray);
    }

    public int numberValues(){
        return values.size();
    }

    public int compareTo(Member other){
        if(fitness<other.fitness){
            // Therefore has worse fitness than other
            return -1;
        }
        if(fitness>other.fitness){
            // Therefore has better fitness than other
            return 1;
        }
        // Equal fitness
        return 0;
    }

    public void mutate(){
        for(int i = 0 ; i<values.size(); i++){
            if(new Random().nextDouble()<Member.mutationRate){
                double value=values.get(i);
                value=mutateValue(value);
                values.set(i,value);
            }
        }
    }

    private double mutateValue(double value){
		double addition =  (new Random().nextDouble()-0.5);
		double newValue = Member.mutationMagnitude*addition + value;
		if(newValue<5&&newValue>-5){
			return newValue;
		}
		return value;
    }

    public void switchValue(int index, Member otherMember){
        double value=otherMember.values.get(index);
        otherMember.values.set(index,values.get(index));
        values.set(index,value);
    }

    public boolean equals(Member other){
        return id==other.id;
    }

    public void print(){
        System.out.print("\n");
        System.out.print(id);
        System.out.print(":{");
        for(double value : values){
            System.out.print(Integer.toString((int) value));
            System.out.print(",");
        }
        System.out.print("}");
    }
}